import { InfiniteScrollerDirective } from './infinite-scroller.directive';
import { ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

class MockElementRef implements ElementRef {
  nativeElement = {};
}
const elRef: ElementRef = null;
beforeEach(() => {
  TestBed.resetTestEnvironment();
  TestBed.initTestEnvironment(BrowserDynamicTestingModule,
        platformBrowserDynamicTesting());
  TestBed.configureTestingModule({
    // ...
    providers: [{provide: ElementRef, useValue: new MockElementRef()}]
  });

  // ...
  // elRef = TestBed.get(ElementRef);
});

describe('InfiniteScrollerDirective', () => {
  it('should create an instance', () => {
    const directive = new InfiniteScrollerDirective(elRef);
    expect(directive).toBeTruthy();
  });
});
