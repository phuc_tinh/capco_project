import { NumberDirective } from './onlynumber.directive';
import { ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';

class MockElementRef implements ElementRef {
  nativeElement = {};
}
let elRef: ElementRef;
beforeEach(() => {
  TestBed.configureTestingModule({
    // ...
    providers: [{provide: ElementRef, useValue: new MockElementRef()}]
  });

  // ...
  elRef = TestBed.get(ElementRef);
});
describe('NumberDirective', () => {
  it('should create an instance', () => {
    const directive = new NumberDirective(elRef);
    expect(directive).toBeTruthy();
  });
});
