import { TestBed } from '@angular/core/testing';

import { PagerService } from './pager.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

describe('PagerService', () => {
  beforeEach(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
        platformBrowserDynamicTesting());
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    const service: PagerService = TestBed.get(PagerService);
    expect(service).toBeTruthy();
  });
});
