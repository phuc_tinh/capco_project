import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor() { }

  private jsonData: any;
  private width: number;
  private height: number;
  private isInfinite: boolean;
  setData(width: number, height: number, isInfinite: boolean, data: string) {

    this.jsonData = JSON.parse(data);
    this.width = width;
    this.height = height;
    this.isInfinite = isInfinite;
  }
  getData(): any {
    return {
      width: this.width,
      height: this.height,
      infinite: this.isInfinite,
      json: this.jsonData
    };
  }
}
