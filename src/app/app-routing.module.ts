import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

export const childRoutes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            { path: '', loadChildren: './component/home/home.module#HomeModule' },
            { path: 'table', loadChildren: './component/table/table.module#TableModule' },
            { path: 'result', loadChildren: './component/result/result.module#ResultModule' },
        ]
    },
    {
      path: '**',
      redirectTo: ''
  }
];

export const routing = RouterModule.forRoot(childRoutes);
