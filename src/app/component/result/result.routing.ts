import { Routes, RouterModule } from '@angular/router';
import { ResultComponent } from '../result/result.component';

const childRoutes: Routes = [
    {
        path: '',
        component: ResultComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
