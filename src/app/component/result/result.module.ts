import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './result.routing';
import { FormsModule } from '@angular/forms';
import { ResultComponent } from './result.component';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule
    ],
    declarations: [
        ResultComponent
    ]
})
export class ResultModule { }
