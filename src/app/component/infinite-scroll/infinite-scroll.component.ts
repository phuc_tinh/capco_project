import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { PagerService } from 'src/app/services/pager.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.sass', '../table/table.component.sass']
})
export class InfiniteScrollComponent implements OnInit, AfterViewInit {

  @Input()
  allItems: any;
  @Input()
  arrKeys: string[] = [];
  @Input()
  tableWidth: number;
  @Input()
  tableHeight: number;

  @Input() childFn: (id: string, status: string, httpService: HttpClient, router: Router) => void;

  currentPage = 0;
  itemPerPage = 20;

  news: Array<any> = [];
  scrollCallback: any;

  constructor(
    private pagerService: PagerService,
    private httpService: HttpClient,
    private router: Router) {
    this.scrollCallback = this.getDataTable.bind(this);
  }

  ngOnInit() { }

  ngAfterViewInit() {
    $(document).ready(() => {
      const hasVScroll = $('#div_body')[0].scrollHeight > $('#div_body').height();
      if (!hasVScroll) {
        const height = $('#div_body table').height();
        $('#div_body').height(height - 1);
      }
      // fix header
      $('#div_header_inner').css('padding-right', '17px');
    });
  }

  getDataTable() {
    if (this.allItems) {
      this.currentPage++;
      const pager = this.pagerService.getPager(this.allItems.length, this.currentPage, +this.itemPerPage);
      // get current page of items
      const pagedItems = this.allItems.slice(pager.startIndex, pager.endIndex + 1);
      this.news = this.news.concat(pagedItems);
    }
    return of(this.news);
  }

  sendClick(id: string, status: string): void {
    this.childFn(id, status, this.httpService, this.router);
  }

}
