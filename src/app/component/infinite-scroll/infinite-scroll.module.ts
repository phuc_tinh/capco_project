import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollComponent } from '../infinite-scroll/infinite-scroll.component';
import { InfiniteScrollerDirective } from 'src/app/directive/infinite-scroller.directive';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        InfiniteScrollComponent,
        InfiniteScrollerDirective
    ]
})
export class InfiniteScrollModule { }
