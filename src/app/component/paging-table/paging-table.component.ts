import { Component, OnInit, Input } from '@angular/core';
import { PagerService } from 'src/app/services/pager.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

declare var $: any;

@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.sass', '../table/table.component.sass']
})
export class PagingTableComponent implements OnInit {

  itemPerPage = 5;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  @Input()
  allItems: string[];
  @Input()
  arrKeys: string[];
  // dataJson: any;
  @Input()
  tableWidth: number;
  @Input()
  tableHeight: number;

  @Input() childFn: (id: string, status: string, httpService: HttpClient, router: Router) => void;

  constructor(private pagerService: PagerService,
              private httpService: HttpClient,
              private router: Router) { }

  ngOnInit() {

    this.setPage(1);
  }

  changeItemPerPage() {
    this.setPage(1);
  }
  fixHeader() {
    const hasVScroll = $('#div_body')[0].scrollHeight > $('#div_body').height();
    if (hasVScroll) {
      $('#div_header_inner').css('padding-right', '17px');
    } else {
      $('#div_header_inner').css('padding-right', '0px');
    }
  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    if (this.allItems) {
      // get pager object from service
      this.pager = this.pagerService.getPager(this.allItems.length, page, +this.itemPerPage);
      // get current page of items
      this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
      // fix padding header
      setTimeout(this.fixHeader, 500);
    }
  }

  sendClick(id: string, status: string): void {
    this.childFn(id, status, this.httpService, this.router);
  }
}
