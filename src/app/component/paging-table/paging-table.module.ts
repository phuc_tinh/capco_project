import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NumberDirective } from 'src/app/directive/onlynumber.directive';
import { PagingTableComponent } from './paging-table.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        PagingTableComponent
    ]
})
export class PagingTableModule { }
