import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingTableComponent } from './paging-table.component';
import { PagerService } from 'src/app/services/pager.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';


describe('PagingTableComponent', () => {
  let component: PagingTableComponent;
  let fixture: ComponentFixture<PagingTableComponent>;

  beforeEach(async(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
        platformBrowserDynamicTesting());
    TestBed.configureTestingModule({
      declarations: [ PagingTableComponent ],
      imports: [FormsModule, RouterTestingModule, HttpClientModule],
      providers: [
        PagerService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
