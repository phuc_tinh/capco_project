import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table.component';

const childRoutes: Routes = [
    {
        path: '',
        component: TableComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
