import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './table.routing';
import { FormsModule } from '@angular/forms';
import { TableComponent } from './table.component';
import { InfiniteScrollComponent } from '../infinite-scroll/infinite-scroll.component';
import { InfiniteScrollerDirective } from 'src/app/directive/infinite-scroller.directive';
import { PagingTableComponent } from '../paging-table/paging-table.component';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule
    ],
    declarations: [
        TableComponent,
        InfiniteScrollComponent,
        PagingTableComponent,
        InfiniteScrollerDirective
    ]
})
export class TableModule { }
