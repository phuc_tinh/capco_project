import { Component, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { PagerService } from '../../services/pager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription, throwError } from 'rxjs';
import { JsonService } from 'src/app/services/json.service';

declare var $: any;

const CHAR_WIDTH = 8;
const FIRST_ITEM_WIDTH = 5;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {

  title = 'Data table loaded';

  allItems: string[];
  arrKeys: string[];
  dataJson: any;
  tableWidth: number;
  tableHeight: number;
  isInfinite: boolean;

  pager: any = {};
  // paged items
  pagedItems: any[];

  constructor(
    private pagerService: PagerService,
    private jsonService: JsonService,
    private router: Router) { }

  ngOnInit() {
    const data = this.jsonService.getData();
    this.dataJson = data.json;

    this.allItems = this.dataJson as string[];
    this.tableWidth = data.width;
    this.tableHeight = data.height;
    this.isInfinite = data.infinite;
    if (this.allItems == null) {
      this.title = 'No data!';
      return;
    }

    const info = this.processData();

    $(document).ready(() => {

      const target = $('.dataTables_scrollHead');
      $('.dataTables_scrollBody').scroll(function() {
        target.prop('scrollLeft', this.scrollLeft);
      });
      this.ajustWidth(info);
    });
  }

  processData() {
    let lengthKey = 0;
    let data = [];
    const dataSet = [];
    // convert to array 2 dimension
    this.dataJson.forEach((val: any, index: number) => {
      const keys = Object.keys(val);
      if (length < keys.length) {
        this.arrKeys = keys;
      }
      lengthKey = keys.length > lengthKey ? keys.length : lengthKey;
      keys.forEach((key) => {
          data.push(val[key]);
      });
      dataSet.push(data);
      data = [];
    });

    return {
      length: lengthKey,
      data: dataSet
    };
  }

  ajustWidth(info: any) {
    // Step1: get data from parameter
    const dataSet: Array<string> = info.data;
    const numberKey: number = info.length;

    // Step2: get max length of each column
    const colsWidth: Array<number> = [];
    colsWidth.push(FIRST_ITEM_WIDTH); // button column
    for (let i = 0; i < numberKey; i++) {
      const maxCol = dataSet.map((value, index) => {
         return (value[i] + '').length * CHAR_WIDTH;
        });
      const max = Math.max.apply(null, maxCol);
      colsWidth.push(max);
    }
    // Step3: set width for each column
    $('#fixed_header').find('tr:first th').each(function(index: number) {
      $(this).width(colsWidth[index]);
      $('#fixed_body').find('tr:first th:nth-child(' + (index + 1) + ')').width(colsWidth[index]);
    });
    // Step4: calculate width of table
    const add = (a: number, b: number) =>  a + b;
    // padding of 1 column : 19px*2 (for left & right)
    const totalWidth: number = colsWidth.reduce(add) + colsWidth.length * (19 * 2);
    $('#fixed_header').width(totalWidth);
    $('#fixed_body').width(totalWidth);
    $('#div_header_inner').width(totalWidth);

  }
  back(): void {
    this.router.navigate(['/']);
  }


  submitClick(id: string, status: string, httpService: HttpClient, router: Router): void {
    const postData = {id, status};
    const headers =  {headers: new  HttpHeaders({ 'Content-Type': 'application/json'})};

    httpService.post('/api/submit', postData, headers).subscribe((data: any) => {
        console.log(data);
        const options: any = {id: data.id, status: data.status};
        /*options = Object.entries(options)
                  .filter(([name, value]) => value !== null)
                  .map(([name, value]) => ({name, value}));*/
        router.navigate(['/result'], { queryParams: options });
      },
      error => {this.handleError(error); }
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

}
