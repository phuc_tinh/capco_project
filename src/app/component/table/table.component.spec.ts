import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { TableModule } from './table.module';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { PagingTableComponent } from '../paging-table/paging-table.component';
import { InfiniteScrollComponent } from '../infinite-scroll/infinite-scroll.component';
import { InfiniteScrollerDirective } from 'src/app/directive/infinite-scroller.directive';
import { RouterTestingModule } from '@angular/router/testing';
import { JsonService } from 'src/app/services/json.service';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
        platformBrowserDynamicTesting());
    TestBed.configureTestingModule({
      declarations: [ TableComponent , PagingTableComponent, InfiniteScrollComponent, InfiniteScrollerDirective],
      imports: [FormsModule, RouterTestingModule, HttpClientModule, RouterModule.forRoot([])],
      providers: [JsonService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title with "No data!"', () => {
    const myComponent = TestBed.createComponent(TableComponent);
    myComponent.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('No data!');
  });

  it('should render testing data for paging table', inject([JsonService], (s: JsonService) => {
    s.setData(500, 300, false, '[{"id":1,"status":"read","name":"David Jane"},{"name":"Nell D. Michael","id":2,"status":"unread"}]');

    const myComponent = TestBed.createComponent(TableComponent);
    myComponent.detectChanges();
    const div = myComponent.nativeElement.querySelector('#pagingParent');
    expect(div).toBeTruthy();
  }));

  it('should render testing data for scroll table', inject([JsonService], (s: JsonService) => {
    s.setData(500, 300, true, '[{"id":1,"status":"read","name":"David Jane"},{"name":"Nell D. Michael","id":2,"status":"unread"}]');

    const myComponent = TestBed.createComponent(TableComponent);
    myComponent.detectChanges();
    const compiled = myComponent.debugElement.nativeElement;
    const div = compiled.querySelector('#infiniteParent');
    expect(div).toBeTruthy();

  }));


});
