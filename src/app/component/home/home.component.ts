import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router,
              private jsonService: JsonService) {

  }

  file: any;
  fileName: string;
  tableWidth: number;
  tableHeight: number;
  errorCode = 0;
  errorMessage: string;
  ngOnInit(): void {
    this.tableWidth = 450;
    this.tableHeight = 200;
  }
  fileChanged(e) {
      if (e.target.files[0]) {// if choose a file
        this.file = e.target.files[0];
        this.fileName = this.file.name.replace(/\\/g, '/').replace(/.*\//, '');
      }
  }

  uploadDocument(isInfinite: boolean) {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      try {
        this.jsonService.setData(
          this.tableWidth as number,
          this.tableHeight as number,
          isInfinite,
          fileReader.result.toString());
      } catch (e) {
        alert('Error: Invalid json file.');
        return;
      }

      this.router.navigate(['/table']);
    };
    this.errorCode = 0; // reset code
    if (!this.validateData()) {
      return;
    }
    if (this.file) {
      fileReader.readAsText(this.file);
    }
  }
  validateData(): boolean {
    // check width,height
    if (this.tableWidth == null || (this.tableWidth + '' === '')) {
      this.errorCode = 1;
      this.errorMessage = 'Enter width';
      return false;
    }
    if (this.tableHeight == null || (this.tableHeight + '' === '')) {
      this.errorCode = 2;
      this.errorMessage = 'Enter height';
      return false;
    }
    if (this.tableWidth < 200) {
      this.errorCode = 1;
      this.errorMessage = 'Width greater than 200px';
      return false;
    }
    if (this.tableHeight < 200) {
      this.errorCode = 2;
      this.errorMessage = 'Height greater than 200px';
      return false;
    }
    if (this.file == null) {
      this.errorCode = 3;
      this.errorMessage = 'Browse json file';
      return false;
    }
    return true;
  }
}
