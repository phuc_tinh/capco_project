import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

const childRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
