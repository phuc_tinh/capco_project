import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { JsonService } from 'src/app/services/json.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterModule } from '@angular/router';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule,
        platformBrowserDynamicTesting());
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [FormsModule, RouterTestingModule, HttpClientModule, RouterModule.forRoot([])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([JsonService], (s: JsonService) => {
    expect(component).toBeTruthy();
  }));

  it('should render input tag (width, height,file) ', async (done) => {
    const myComponent = TestBed.createComponent(HomeComponent);
    myComponent.detectChanges();
    const compiled = myComponent.debugElement.nativeElement;
    myComponent.whenStable().then(() => {
      let input = compiled.querySelector('#tableWidth');
      expect(input.value).toEqual('450');

      input = compiled.querySelector('#tableHeight');
      expect(input.value).toBe('200');

      input = compiled.querySelector('#file');
      expect(input.type).toBe('file');
    });
    done();
  });
});
