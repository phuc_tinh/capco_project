import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './home.routing';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { NumberDirective } from 'src/app/directive/onlynumber.directive';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule
    ],
    declarations: [
        HomeComponent,
        NumberDirective
    ]
})
export class HomeModule { }
