**Demo table component**

This is a demo for displaying data on the table component.You can select the width, height of the table, and select the json file. Then click the button displayed to display the paging table, or the infinite scroll table

## Environment

Here is development environment

1. nodeJs : 10.8.0
2. Agular CLI : 8.0.6
3. Angular : 8.0.3

## Start project

You can use the commands line below:

1. Change to current project folder, use command: npm install
2. Continue with this command: ng serve.
3. Navigate http://localhost:4444 in browser. You can access to home page.

## Manual test sequence

You can start by following the steps below:

1. Change width, height (value must be greater than 200px).
2. Browse json file.
3. Press "Normal Table" or "Infinite Table" button.
    * "Normal Table": paging table
    * "Infinite Table": infinite scroll table
4. The result can show on the screen
5. Press "+" button on the table, the result page will be navigate. It shows id and status
